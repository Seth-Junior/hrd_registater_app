import React, { useState, useEffect, Component } from 'react'
import { StyleSheet, Text, View, Alert, ScrollView, ImageBackground, TouchableOpacity } from 'react-native'
import { Formik } from 'formik';
import * as Yup from 'yup';
import { HStack, Radio, Input, Select, Button, VStack } from 'native-base'
import { useDispatch, useSelector } from 'react-redux';
import { getAllProvince } from '../../redux/actions/provinceAction';
import { getAllUniversities } from '../../redux/actions/universityAction';
import { add_student } from '../../services/student.service';
import DatePicker from 'react-native-date-picker'
import BottomSheet from 'reanimated-bottom-sheet';
import Animated from 'react-native-reanimated';
import ImagePickerCropper from 'react-native-image-crop-picker';
import { upload_image } from '../../services/image.service';

const EmergencyContactRelationship = [
    { id: 1, fullname: 'FATHER' },
    { id: 2, fullname: 'MOTHER' },
    { id: 3, fullname: 'BROTHER' },
    { id: 4, fullname: 'SISTER' },
    { id: 5, fullname: 'UNCLE' },
    { id: 6, fullname: 'AUNT' },
    { id: 7, fullname: 'OTHER' }
];
const Educations = [
    { id: 1, fullname: 'YEAR3_SEMESTER1' },
    { id: 2, fullname: 'YEAR3_SEMESTER2' },
    { id: 3, fullname: 'YEAR4_SEMESTER1' },
    { id: 4, fullname: 'YEAR4_SEMESTER2' },
    { id: 5, fullname: 'GRADUATED' },
]

const Sigin = () => {

    const [isPickerShow, setIsPickerShow] = useState(false);
    const [imageFile, setImageFile] = useState(null);
    const [date, setDate] = useState(new Date(Date.now()));
    const [imagePath, setImagePath] = useState('https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_1280.png')

    dispatch = useDispatch()
    const { province } = useSelector(state => state.provinceReducer);
    const { universities } = useSelector(state => state.universityReducer);

    // console.log("All province : ",province);
    // console.log("All universities : ",universities);

    useEffect(() => {
        dispatch(getAllProvince())
        dispatch(getAllUniversities())
    }, [])

    const birthDate = `${date.toLocaleDateString()}`;
    console.log("THIS IS BRITH DATE : ", birthDate);
    console.log("THIS IS BRITH DATE 22222: ", birthDate);
    const [dob, setDob] = useState(birthDate);
    console.log("THIS IS BRITH DATE 33333: ", dob);


    const showPicker = () => {
        setIsPickerShow(true);
    };

    // ----- Choos Image -------- 
    const takePhotoFromCamera = () => {
        ImagePickerCropper.openCamera({
            width: 300,
            height: 300,
            cropping: true,
            freeStyleCropEnabled: true,
        }).then(image => {
            console.log(image);
            setImagePath(image.path);
            bs.current.snapTo(1);
        });
    }

    const choosePhotoFromLibrary = () => {
        ImagePickerCropper.openPicker({
            width: 300,
            height: 300,
            cropping: true,
            freeStyleCropEnabled: true,
        }).then(image => {
            console.log('Image : ', image);
            setImagePath(image.path);
            setImageFile(image);
            bs.current.snapTo(1);
        });
    }


    renderInner = () => (
        <View style={styles.panel}>
            <View style={{ alignItems: 'center', height: '15%' }}>
                <Text style={styles.panelTitle}>Choose your Photo</Text>
            </View>
            <TouchableOpacity style={styles.panelButton} onPress={takePhotoFromCamera}>
                <Text style={styles.panelButtonTitle}>Take Photo</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.panelButton} onPress={choosePhotoFromLibrary}>
                <Text style={styles.panelButtonTitle}>Choose From Library</Text>
            </TouchableOpacity>
            <TouchableOpacity
                style={styles.panelButton}
                onPress={() => bs.current.snapTo(1)}
            >
                <Text style={styles.panelButtonTitle}>Cancel</Text>
            </TouchableOpacity>
        </View>
    );

    renderHeader = () => (
        <View style={styles.header}>
            <TouchableOpacity onPress={() => bs.current.snapTo(1)}>
                <View style={styles.panelHeader}>
                    <View style={styles.panelHandle} />
                </View>
            </TouchableOpacity>
        </View>
    );

    bs = React.createRef();
    fall = new Animated.Value(1);


    return (
        <>
            <ScrollView style={styles.container}>
                <BottomSheet
                    ref={bs}
                    snapPoints={[330, 0]}
                    renderContent={renderInner}
                    renderHeader={renderHeader}
                    initialSnap={1}
                    callbackNode={fall}
                    enabledGestureInteraction={true}
                />
                <Animated.View style={{
                    // margin: 20,
                    opacity: Animated.add(0.3, Animated.multiply(fall, 1.0)),
                }}>
                    <Text style={styles.title}> Application Form </Text>
                    <Text style={styles.subTitle}>Please complete all your information</Text>
                    <Formik
                        initialValues={
                            {
                                currentAddressId: "",
                                dateOfBirth: new Date(),
                                education: "",
                                email: "",
                                emergencyContact: "",
                                emergencyContactRelationship: "",
                                fullname: "",
                                gender: "male",
                                nationalId: "",
                                phone: "",
                                photo: "",
                                placeOfBirthId: "",
                                question: "",
                                universityId: ""
                            }
                        }
                        validationSchema={Yup.object({
                            fullname: Yup.string()
                                .required('Please input your fullname'),
                            email: Yup.string()
                                .email('Please input invalid email')
                                .required('Please input your email address'),
                            dateOfBirth: Yup.string()
                                .required('Please select your date of birth'),
                            phone: Yup.string()
                                .required('Please input your phone number'),
                            emergencyContact: Yup.string()
                                .required('Please input your emergency contact'),
                            emergencyContactRelationship: Yup.string()
                                .required('Please input your emergency contact relation'),
                            placeOfBirthId: Yup.string()
                                .required('Please select your place of birth'),
                            currentAddressId: Yup.string()
                                .required('Please select your current address'),
                            universityId: Yup.string()
                                .required('Please select your university'),
                            education: Yup.string()
                                .required('Please select your education'),
                        })}
                        onSubmit={(values, formikActions) => {

                            if (imagePath) {
                                upload_image(imageFile)
                                    .then(result => {
                                        console.log('result : ', result);
                                        const currentAddId = province.filter(
                                            item => item.fullname == values.currentAddressId
                                        );
                                        const placeId = province.filter(
                                            item => item.fullname == values.placeOfBirthId
                                        );
                                        const univerId = universities.filter(
                                            item => item.fullname == values.universityId
                                        );

                                        // console.log("ID : ", placeId, currentAddId, univerId);

                                        const student = {
                                            "currentAddressId": currentAddId[0].id,
                                            "dateOfBirth": dob,
                                            "education": values.education,
                                            "email": values.email,
                                            "emergencyContact": values.emergencyContact,
                                            "emergencyContactRelationship": values.emergencyContactRelationship.toUpperCase(),
                                            "fullname": values.fullname,
                                            "gender": values.gender.toUpperCase(),
                                            "nationalId": "",
                                            "phone": values.phone,
                                            "photo": `${result.fullpath}`,
                                            "placeOfBirthId": placeId[0].id,
                                            "question": "",
                                            "universityId": univerId[0].id,
                                        }

                                        console.log("Students :", student);

                                        add_student(student)
                                            .then(result => {
                                                Alert.alert(
                                                    'Register successfully !',
                                                )
                                                console.log('Student data : ', student);
                                            })
                                            .catche(err => {
                                                Alert.alert(
                                                    'Register failed'
                                                )
                                            }
                                            )
                                    }).catch(err => {
                                        console.log('upload image error', err);
                                    })
                            }
                            else {
                                console.log('Photo is requie');
                            }

                            formikActions.setSubmitting(false);
                            // resetForm()
                        }}
                    >
                        {props => (console.log(props) ||
                            <View>

                                {/*----------------- Full name -------------- */}
                                <Text style={styles.labelStyle}>
                                    Full Name
                                    <Text style={{ color: 'red' }}> *</Text>
                                </Text>
                                <Input
                                    onChangeText={props.handleChange('fullname')}
                                    onBlur={props.handleBlur('fullname')}
                                    value={props.values.fullname}
                                    placeholder="Full Name "
                                    style={styles.input}
                                    _focus={{ borderColor: 'primary.800' }}
                                />
                                {props.touched.fullname && props.errors.fullname ? (
                                    <Text style={styles.error}>{props.errors.fullname}</Text>
                                ) : null}

                                {/*----------------- gernder-------------- */}
                                <Text style={styles.labelStyle}>
                                    Gender
                                    <Text style={{ color: 'red' }}> *</Text>
                                </Text>
                                <View style={{ marginTop: 8 }}>
                                    <Radio.Group
                                        name="myRadioGroup"
                                        value={props.values.gender}
                                        onChange={props.handleChange('gender')}
                                    >
                                        <HStack space={4}>
                                            <Radio value="male" my={1}>
                                                Male
                                            </Radio>
                                            <Radio value="female" my={1}>
                                                Female
                                            </Radio>
                                        </HStack>
                                    </Radio.Group>
                                </View>

                                {/*----------------- Date of birth -------------- */}
                                <Text style={styles.labelStyle}>
                                    Date of birth
                                    <Text style={{ color: 'red' }}> *</Text>
                                </Text>
                                <Button
                                    borderRadius='5'
                                    h='10'
                                    style={styles.pickedDateContainer}
                                    onPress={showPicker}
                                    variant="unstyled"

                                >
                                    <View>
                                        {Platform.OS === "android" ?
                                            <HStack w="80" justifyContent="space-between" >
                                                <Text ml={1}>{birthDate}</Text>
                                            </HStack> :
                                            <HStack w="80" justifyContent="space-between">
                                                <Text ml="1.5">{birthDate}</Text>
                                                <Text mr="1.5">{''}</Text>
                                            </HStack>
                                        }
                                    </View>
                                </Button>
                                {isPickerShow && (
                                    <DatePicker
                                        androidVariant='iosClone'
                                        value={props.values.dateOfBirth}
                                        modal
                                        mode="date"
                                        open={isPickerShow}
                                        date={date}
                                        onDateChange={val => props.setFieldValue('dateOfBirth', val)}
                                        onConfirm={(date) => {
                                            setIsPickerShow(false)
                                            setDate(date)
                                            setDob(date)
                                        }}
                                        onCancel={() => {
                                            setIsPickerShow(false)
                                        }}
                                    />

                                )}
                                {props.touched.dateOfBirth && props.errors.dateOfBirth ? (
                                    <Text style={styles.error}>{props.errors.dateOfBirth}</Text>
                                ) : null}

                                {/* ---------------- Place of Birth ----------- */}
                                <Text style={styles.labelStyle}>
                                    Place of Birth
                                    <Text style={{ color: 'red' }}> *</Text>
                                </Text>
                                <Select
                                    value={props.values.placeOfBirthId}
                                    onValueChange={props.handleChange('placeOfBirthId')}
                                    minWidth="200"
                                    accessibilityLabel="Choose Service"
                                    placeholder="Select Place of Birth"
                                    fontSize="md"
                                    marginBottom="2"
                                    _selectedItem={{
                                        bg: "teal.600",
                                        // endIcon: <CheckIcon size={5} />,
                                    }}
                                    marginTop={2}
                                    style={styles.selectStyle}
                                >
                                    {
                                        province?.map((val) =>
                                            <Select.Item key={val.id} label={val.fullname} value={val.fullname} />
                                        )
                                    }
                                </Select>
                                {props.touched.placeOfBirthId && props.errors.placeOfBirthId ? (
                                    <Text style={styles.error}>{props.errors.placeOfBirthId}</Text>
                                ) : null}

                                {/* ---------------- Current Address ----------- */}
                                <Text style={styles.labelStyle}>
                                    Current Address
                                    <Text style={{ color: 'red' }}> *</Text>
                                </Text>
                                <Select
                                    value={props.values.currentAddressId}
                                    onValueChange={props.handleChange('currentAddressId')}
                                    minWidth="200"
                                    accessibilityLabel="Choose Service"
                                    placeholder="Select Current Address"
                                    fontSize='md'
                                    _selectedItem={{
                                        bg: "teal.600",
                                        // endIcon: <CheckIcon size={5} />,
                                    }}
                                    marginTop={2}
                                    style={styles.selectStyle}
                                >
                                    {
                                        province?.map((val) =>
                                            <Select.Item key={val.id} label={val.fullname} value={val.fullname} style={{ fontSize: 10 }} />
                                        )
                                    }
                                </Select>
                                {props.touched.currentAddressId && props.errors.currentAddressId ? (
                                    <Text style={styles.error}>{props.errors.currentAddressId}</Text>
                                ) : null}

                                {/* ------------ Email ------------------ */}
                                <Text style={styles.labelStyle}>
                                    Email Address
                                    <Text style={{ color: 'red' }}> *</Text>
                                </Text>
                                <Input
                                    onChangeText={props.handleChange('email')}
                                    onBlur={props.handleBlur('email')}
                                    value={props.values.email}
                                    placeholder="Email Address"
                                    style={styles.input}
                                    _focus={{ borderColor: 'primary.800' }}
                                />
                                {props.touched.email && props.errors.email ? (
                                    <Text style={styles.error}>{props.errors.email}</Text>
                                ) : null}

                                {/* ------------ Phone Number ------------------ */}
                                <Text style={styles.labelStyle}>
                                    Phone Number
                                    <Text style={{ color: 'red' }}> *</Text>
                                </Text>
                                <Input
                                    onChangeText={props.handleChange('phone')}
                                    onBlur={props.handleBlur('phone')}
                                    value={props.values.phone}
                                    placeholder="Phone Number"
                                    style={styles.input}
                                    _focus={{ borderColor: 'primary.800' }}
                                />
                                {props.touched.phone && props.errors.phone ? (
                                    <Text style={styles.error}>{props.errors.phone}</Text>
                                ) : null}

                                {/* ------------ Emergency Contact ------------------ */}
                                <Text style={styles.labelStyle}>
                                    Emergency Contact
                                    <Text style={{ color: 'red' }}> *</Text>
                                </Text>
                                <Input
                                    onChangeText={props.handleChange('emergencyContact')}
                                    onBlur={props.handleBlur('emergencyContact')}
                                    value={props.values.emergencyContact}
                                    placeholder="Emergency Contact"
                                    style={styles.input}
                                    _focus={{ borderColor: 'primary.800' }}
                                />
                                {props.touched.emergencyContact && props.errors.emergencyContact ? (
                                    <Text style={styles.error}>{props.errors.emergencyContact}</Text>
                                ) : null}

                                {/* ------------ Emergency Contact Relationship ------------------ */}
                                <Text style={styles.labelStyle}>
                                    Emergency Contact Relationship
                                    <Text style={{ color: 'red' }}> *</Text>
                                </Text>
                                <Select
                                    value={props.values.emergencyContactRelationship}
                                    onValueChange={props.handleChange('emergencyContactRelationship')}
                                    minWidth="200"
                                    placeholder="Select Your Emergenct Contact Relationship"
                                    fontSize="md"
                                    _selectedItem={{
                                        bg: "teal.600",
                                    }}
                                    marginTop={4}
                                    style={styles.selectStyle}
                                >
                                    {
                                        EmergencyContactRelationship?.map((val) =>
                                            <Select.Item
                                                key={val.id}
                                                label={val.fullname}
                                                value={val.fullname}
                                            />
                                        )
                                    }
                                </Select>
                                {props.touched.emergencyContactRelationship && props.errors.emergencyContactRelationship ? (
                                    <Text style={styles.error}>{props.errors.emergencyContactRelationship}</Text>
                                ) : null}

                                {/* ------------ Education ------------------ */}
                                <Text style={styles.labelStyle}>
                                    Education
                                    <Text style={{ color: 'red' }}> *</Text>
                                </Text>
                                <Select
                                    value={props.values.education}
                                    onValueChange={props.handleChange('education')}
                                    minWidth="200"
                                    placeholder="Select Your Education"
                                    fontSize="md"
                                    _selectedItem={{
                                        bg: "teal.600",
                                    }}
                                    marginTop={4}
                                    style={styles.selectStyle}
                                >
                                    {
                                        Educations?.map((val) =>
                                            <Select.Item
                                                key={val.id}
                                                label={val.fullname}
                                                value={val.fullname}
                                            />
                                        )
                                    }
                                </Select>
                                {props.touched.education && props.errors.education ? (
                                    <Text style={styles.error}>{props.errors.education}</Text>
                                ) : null}

                                {/* ------------ University ------------------ */}
                                <Text style={styles.labelStyle}>
                                    University
                                    <Text style={{ color: 'red' }}> *</Text>
                                </Text>
                                <Select
                                    value={props.values.universityId}
                                    onValueChange={props.handleChange('universityId')}
                                    minWidth="200"
                                    placeholder="Select University"
                                    fontSize="md"
                                    _selectedItem={{
                                        bg: "teal.600",
                                    }}
                                    marginTop={4}
                                    style={styles.selectStyle}
                                >
                                    {
                                        universities?.map((val) =>
                                            <Select.Item
                                                key={val.id}
                                                label={val.fullname}
                                                value={val.fullname}
                                            />
                                        )
                                    }
                                </Select>
                                {props.touched.universityId && props.errors.universityId ? (
                                    <Text style={styles.error}>{props.errors.universityId}</Text>
                                ) : null}

                                {/* ------------ Image ------------ */}
                                <View>
                                    <View style={{ marginTop: 70, alignItems: 'center', justifyContent: 'center' }}>
                                        <TouchableOpacity onPress={() => bs.current.snapTo(0)}>
                                            <View
                                                style={{
                                                    height: 100,
                                                    width: 100,
                                                    borderRadius: 15,
                                                    justifyContent: 'center',
                                                    alignItems: 'center',
                                                }}>
                                                <ImageBackground
                                                    source={{
                                                        uri: imagePath,
                                                    }}
                                                    style={{ height: 200, width: 200, overflow: 'hidden' }}
                                                    imageStyle={{ borderRadius: 100 }}>
                                                    <View
                                                        style={{
                                                            flex: 1,
                                                            justifyContent: 'center',
                                                            alignItems: 'center',
                                                        }}>
                                                    </View>
                                                </ImageBackground>
                                            </View>
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={() => bs.current.snapTo(0)} style={{ marginTop: 25, fontSize: 18, fontWeight: 'bold', backgroundColor: 'rgba(255, 255, 255, 0.8)', borderRadius: 20, padding: 10 }}>
                                            <Text>Choose Image</Text>
                                        </TouchableOpacity>
                                    </View>

                                </View>

                                <View style={styles.btnStyle}>
                                    <Button
                                        onPress={props.handleSubmit}
                                        mode="contained"
                                        loading={props.isSubmitting}
                                        disabled={props.isSubmitting}
                                        backgroundColor="primary.700"
                                    >
                                        Register
                                    </Button>
                                    <Button
                                        onPress={props.handleReset}
                                        mode="contained"
                                        backgroundColor="primary.600"
                                        marginTop={2}
                                        marginBottom={2}
                                    >
                                        Clear All
                                    </Button>
                                </View>

                            </View>
                        )}
                    </Formik>
                </Animated.View>
            </ScrollView>
        </>
    )
}

export default Sigin

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        padding: 20,
        marginTop: 10
    },
    title: {
        fontSize: 30,
        fontWeight: '400'
    },
    subTitle: {
        marginVertical: 10
    },
    error: {
        color: 'red',
        marginHorizontal: 8,
        marginTop: 10
    },
    input: {
        height: 50,
        width: '100%',
        borderWidth: 1,
        marginTop: 15,
        fontSize: 15
    },
    selectStyle: {
        marginVertical:  Platform.OS === 'ios'? 8 : 2 ,
        fontSize: 15
    },
    btnStyle: {
        marginVertical: 20
    },
    labelStyle: {
        fontSize: 15,
        fontWeight: '600',
        marginTop: 15
    },
    pickedDateContainer: {
        marginTop: 15,
        width: '100%',
        borderWidth: 0.8,
        color: 'red',
        borderColor: '#CBCBCB',
        height:50
    },
    // ----- Image ------- 
    panel: {
        padding: 20,
        backgroundColor: '#FFFFFF',
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        shadowColor: '#000000',
        shadowOffset: { width: 0, height: 0 },
        shadowRadius: 5,
        shadowOpacity: 0.4,
    },
    header: {
        backgroundColor: '#ffffff',
        shadowColor: '#333333',
        shadowOffset: { width: -1, height: -3 },
        shadowRadius: 2,
        shadowOpacity: 0.4,
        paddingTop: 20,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
    },
    panelHeader: {
        alignItems: 'center',
    },
    panelHandle: {
        width: 40,
        height: 8,
        borderRadius: 4,
        backgroundColor: '#00000040',
    },
    panelTitle: {
        fontSize: 22,
        height: 30,
    },
    panelButton: {
        padding: 13,
        borderRadius: 10,
        backgroundColor: '#FF6347',
        alignItems: 'center',
        marginVertical: 7,
    },
    panelButtonTitle: {
        fontSize: 17,
        fontWeight: 'bold',
        color: 'white',
    },
})
