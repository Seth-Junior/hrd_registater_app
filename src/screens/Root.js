import React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { View, Text, Image, StyleSheet, SafeAreaView } from 'react-native'
import Sigin from './sigin/Sigin'
import { createStackNavigator } from '@react-navigation/stack'


const Stack = createStackNavigator();

const Root = () => {
    return (
        <SafeAreaView
            style={{ flex: 1 }}
        >
            <NavigationContainer>
                <Stack.Navigator>
                    <Stack.Screen name="SingIn" component={Sigin}
                        options={{
                            headerLeft: (() => (
                                <Image
                                    style={styles.logoStyle}
                                    resizeMode="contain"
                                    source={{
                                        uri: 'https://kshrd.com.kh/static/media/logo.f368c431.png'
                                    }}
                                />
                            )),
                            headerStatusBarHeight: 40,
                            headerTitle: (() => (
                                <View style={styles.headerTitle}>
                                    <Text style={styles.headerTextStyle}>មជ្ឈមណ្ឌលកូរ៉េ សហ្វវែរ អេច អ ឌី</Text>
                                    <Text>Korea Software HRD Center</Text>
                                </View>
                            )),
                        }}
                    />
                </Stack.Navigator>
            </NavigationContainer>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    headerTitle:{
        height:'100%',
        // backgroundColor:'red',
        marginBottom:40,
    },
    headerTextStyle: {
        fontSize: 17
    },
    logoStyle: {
        width: 70,
        height: 60,
        marginBottom:40,
    },
})

export default Root
