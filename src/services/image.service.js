import { Platform } from "react-native";
import { API } from "../utils/api";

export const upload_image = async (image) => {
    try {
        const imgPath = `${image.path}`.split('/')
        const imgName = imgPath[imgPath.length - 1]
        const formData = new FormData();
        formData.append(
            'file',
            {
                uri: image.path,
                name: Platform.OS === 'ios' ? image.filename : imgName,
                type: image.mime,
            }
        );
        const result = await API.post('files/upload',formData)
        console.log('upload_image_result:',result.data);
        return result.data;
    } catch (error) {
        console.log('upload_image_error',error);
    }
}