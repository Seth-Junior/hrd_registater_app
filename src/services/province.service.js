import { API } from '../utils/api'

export const get_all_province = async () => {
    try {
        const provinces = await API.get('/provinces');
        // console.log("Province",provinces);
        return provinces.data.payload
    } catch (error) {
        console.log('Error');
    }
} 