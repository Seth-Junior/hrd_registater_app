import { API } from "../utils/api";

export const add_student = async (studentPost) => {
    const addStudent = await API.post('/students', studentPost)
    console.log(" addStudent.Data : ", addStudent.data)
    return addStudent.data
}