import { API } from "../utils/api";

export const get_all_universities = async () =>{
    try {
        const allUniversities = await API.get('/universities')
        // console.log("University:",allUniversities.data.payload);
        return allUniversities.data.payload
    } catch (error) {
        console.log('get university error !');
    }
}