import React, { useState } from "react"
import  ImagePickerCropper from 'react-native-image-crop-picker';
import { ImageBackground, StyleSheet, TouchableOpacity, View , Text} from "react-native";

import BottomSheet from 'reanimated-bottom-sheet';
import Animated from 'react-native-reanimated';

const Example = () => {
    const [imagePath, setImagePath] = useState('https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_1280.png')
    // const { colors } = useTheme();
    const takePhotoFromCamera = () => {
        ImagePickerCropper.openCamera({
            width: 300,
            height: 300,
            cropping: true,
            freeStyleCropEnabled: true,
        }).then(image => {
            console.log(image);
            setImagePath(image.path);
            bs.current.snapTo(1);
        });
    }

    const choosePhotoFromLibrary = () => {
        ImagePickerCropper.openPicker({
            width: 300,
            height: 300,
            cropping: true,
            freeStyleCropEnabled: true,
        }).then(image => {
            console.log(image);
            setImagePath(image.path);
            bs.current.snapTo(1);
        });
    }

    renderInner = () => (
        <View style={styles.panel}>
            <View style={{ alignItems: 'center' , height: '15%'}}>
                <Text style={styles.panelTitle}>Choose your Photo</Text>
            </View>
            <TouchableOpacity style={styles.panelButton} onPress={takePhotoFromCamera}>
                <Text style={styles.panelButtonTitle}>Take Photo</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.panelButton} onPress={choosePhotoFromLibrary}>
                <Text style={styles.panelButtonTitle}>Choose From Library</Text>
            </TouchableOpacity>
            <TouchableOpacity
                style={styles.panelButton}
                onPress={() => bs.current.snapTo(1)}
            >
                <Text style={styles.panelButtonTitle}>Cancel</Text>
            </TouchableOpacity>
        </View>
    );

    renderHeader = () => (
        <View style={styles.header}>
        <TouchableOpacity onPress={() => bs.current.snapTo(1)}>
            <View style={styles.panelHeader}>
                <View style={styles.panelHandle} />
            </View>
            </TouchableOpacity>
        </View>
    );

    bs = React.createRef();
    fall = new Animated.Value(1);

    return (
        <View>
            <BottomSheet
                ref={bs}
                snapPoints={[330, 0]}
                renderContent={renderInner}
                renderHeader={renderHeader}
                initialSnap={1}
                callbackNode={fall}
                enabledGestureInteraction={true}
            />
            <Animated.View style={{
                margin: 20,
                opacity: Animated.add(0.3, Animated.multiply(fall, 1.0)),
            }}>
                <View style={{marginTop: 100, alignItems: 'center' ,justifyContent: 'center'}}>
                    <TouchableOpacity onPress={() => bs.current.snapTo(0)}>
                        <View
                            style={{
                                height: 100,
                                width: 100,
                                borderRadius: 15,
                                justifyContent: 'center',
                                alignItems: 'center',
                            }}>
                            <ImageBackground
                                source={{
                                    uri: imagePath,
                                }}
                                style={{ height: 200, width: 200 , overflow: 'hidden'}}
                                imageStyle={{ borderRadius: 100 }}>
                                <View
                                    style={{
                                        flex: 1,
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                    }}>
                                </View>
                            </ImageBackground>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => bs.current.snapTo(0)} style={{ marginTop: 25, fontSize: 18, fontWeight: 'bold', backgroundColor: 'rgba(255, 255, 255, 0.8)', borderRadius: 20, padding: 10 }}>
                        <Text>Choose Image</Text>
                    </TouchableOpacity>
                </View>
            </Animated.View>
        </View>
    )
}

const styles = StyleSheet.create({
    header: {
        backgroundColor: '#ffffff',
        shadowColor: '#333333',
        shadowOffset: { width: -1, height: -3 },
        shadowRadius: 2,
        shadowOpacity: 0.4,
        paddingTop: 20,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
    },
    panelHeader: {
        alignItems: 'center',
    },
    panelHandle: {
        width: 40,
        height: 8,
        borderRadius: 4,
        backgroundColor: '#00000040',
        marginBottom: 6,
    },
    panelTitle: {
        fontSize: 22,
        height: 30,
    },
    panelButton: {
        padding: 13,
        borderRadius: 10,
        backgroundColor: '#FF6347',
        alignItems: 'center',
        marginVertical: 7,
    },
    panelButtonTitle: {
        fontSize: 17,
        fontWeight: 'bold',
        color: 'white',
    },
});

export default Example;