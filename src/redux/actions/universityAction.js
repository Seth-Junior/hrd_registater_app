import { get_all_universities } from "../../services/university.service";
 
export const GET_ALL_UNIVERSITY = 'GET_ALL_UNIVERSITY'

export const getAllUniversities = () => async (dispatch) =>{
    const allUniversities = await get_all_universities();

    dispatch({
        type : GET_ALL_UNIVERSITY,
        payload : allUniversities
    })

}