import { get_all_province } from "../../services/province.service";

export const GET_ALL_PROVINCE = 'GET_ALL_PROVINCE'

export const getAllProvince = () => async (dispatch) => {
    const provinces = await get_all_province()

    dispatch({
        type: GET_ALL_PROVINCE,
        payload: provinces
    })
}