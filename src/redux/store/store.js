import { combineReducers , applyMiddleware , createStore } from 'redux';
// import rootReducer from '../reducers/Root';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import { provinceReducer } from '../reducers/provinceReducer'
import { universityReducer } from '../reducers/universityReducer';

const rootReducer = combineReducers({
    provinceReducer,
    universityReducer
})

export const store = createStore(rootReducer,applyMiddleware(thunk,logger))