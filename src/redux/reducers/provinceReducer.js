import { GET_ALL_PROVINCE } from "../actions/provinceAction";

const initailState = {
    province : []
}

export const provinceReducer = (state = initailState , action ) => {
    switch (action.type) {
        case GET_ALL_PROVINCE:
            return{
                ...state,
                province:action.payload
            }
            break;
    
        default: return state;
            break;
    }
}