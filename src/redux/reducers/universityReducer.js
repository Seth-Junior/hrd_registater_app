import { GET_ALL_UNIVERSITY } from "../actions/universityAction";

const initailState ={
    universities : []
}

export const universityReducer = (state = initailState , action) => {
    switch (action.type) {
        case GET_ALL_UNIVERSITY:
            return{
                ...state,
                universities: action.payload
            }
            break;
    
        default: return state ;
            break;
    }
}