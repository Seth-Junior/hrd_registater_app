import React from 'react'
import 'react-native-gesture-handler';
import { StyleSheet, Text, View } from 'react-native'
import Root from './src/screens/Root';
import { NativeBaseProvider } from 'native-base'
import { Provider } from 'react-redux';
import { store } from './src/redux/store/store';

const App = () => {
  return (
    <Provider store={store}>
      <NativeBaseProvider>
        <Root />
      </NativeBaseProvider>
    </Provider>
  )
}

export default App

const styles = StyleSheet.create({})
